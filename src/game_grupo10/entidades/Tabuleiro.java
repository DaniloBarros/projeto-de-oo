package game_grupo10.entidades;

import java.util.ArrayList;


public class Tabuleiro {
	
	private ArrayList<Carta> cemiterio = new ArrayList<Carta>();
	private ArrayList<Carta> field = new ArrayList<Carta>();
	private ArrayList<Carta> terreno = new ArrayList<Carta>();
	private ArrayList<Carta> magia = new ArrayList<Carta>();

	private Jogador jogador;
	
	public Tabuleiro(Jogador jogador) {
		
		this.jogador=jogador;
	}
	
	public void invocarCarta(String classe, Carta carta){
		if(classe.equals("game_grupo10.entidades.Terreno")){
			terreno.add(carta);
		}else if(classe.equals("game_grupo10.entidades.Magia")){
			magia.add(carta);
		}else{
			field.add(carta);
		}
	}
	
	public void adicionarCemiterio(Carta carta){
		cemiterio.add(carta);
	}
	
	public String listaCemiterio(){
		
		StringBuilder str = new StringBuilder("");
		
		str.append("\n\tCartas do Cemiterio: \n");
				
		if (cemiterio.size()>0) {
			for (int i = cemiterio.size() - 1; i >= 0; i--) {
				str.insert(str.length(), cemiterio.get(i).toString());
				str.insert(str.length(),"\n\n");
			}
		}
		
		return str.toString();
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder("");
		
		str.append("\n\tCartas do Campo: \n");
		str.append("\tMonstros: \n");
		if (field.size()>0) {
			for (int i = field.size() - 1; i >= 0; i--) {
				str.insert(str.length(), field.get(i).toString());
				str.insert(str.length(),"\n\n");
			}
		}
		str.append("\n\tMagia: \n");
		if (magia.size()>0) {
			for (int i = magia.size() - 1; i >= 0; i--) {
				str.insert(str.length(), magia.get(i).toString());
				str.insert(str.length(),"\n\n");
			}
		}
		str.append("\n\tTerreno: \n");
		if (terreno.size()>0) {
			for (int i = terreno.size() - 1; i >= 0; i--) {
				str.insert(str.length(), terreno.get(i).toString());				
				str.insert(str.length(),"\n");
			}
		}
		
		return str.toString();
	}

	public ArrayList<Carta> getCemiterio() {
		return cemiterio;
	}

	public void setCemiterio(ArrayList<Carta> cemiterio) {
		this.cemiterio = cemiterio;
	}

	public ArrayList<Carta> getField() {
		return field;
	}

	public void setField(ArrayList<Carta> field) {
		this.field = field;
	}

	public Jogador getJogador() {
		return jogador;
	}

	public void setJogador(Jogador jogador, int senha) {
		if(senha==1324)
			this.jogador = jogador;
	}
	
	

}
