package game_grupo10.entidades;


public class Monstro extends Carta {
	
	private String nome;
	private int atk;
	private int def;
	private int num;

	public Monstro(int num) {
		super(num);
		this.atk = 0;
		this.def = 0;
		this.nome = "vazio";
		this.num = num;
		
	}
	
	@Override
	public void criarCarta(String nome, int atk, int def, int num){
		
		this.atk = atk;
		this.def = def;
		this.nome = nome;	
		super.serie = num;

		boolean confirm=this.validacao(num, getClass().getName());
		
		if(confirm==false){			
			this.atk = 0;
			this.def = 0;
			this.nome = "vazio";
			this.num = super.serie = -1;
		}
	}
	
	@Override
	public String toString(){
		return String.format("%s:\nAttack: %d\nDefense: %d\nSerie Number: %d", this.nome,
				this.atk, this.def, this.serie);
	}
	@Override
	public String Nome() {
		return nome;
	}
	
	public int getNum(){
		return num;
	}
	
	public void setNum(int num){
		this.num = num;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getAtk() {
		return atk;
	}

	public void setAtk(int atk) {
		this.atk = atk;
	}
	
	public int getDef() {
		return def;
	}

	public void setDef(int def) {
		this.def = def;
	}
	
	

}
