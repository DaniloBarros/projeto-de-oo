package game_grupo10.entidades;

public class Carta {

	private int senha = 1234;
	protected int serie;
	protected boolean tipo;
	
	private Deck deck;
	private Jogador jogador;
	
	
	public Carta(int serie) {
		this.serie=serie;
	}
	
	//Polimorfismo
	
	//*********Terrreno********
	public void criarCarta(int mana, int num){
		boolean confirm=this.validacao(num, getClass().getName());
		if(confirm){
			
		}
	}
	
	//*********Monstro*********
	public void criarCarta(String nome, int atk, int def, int num){
		boolean confirm=this.validacao(num, getClass().getName());
		if(confirm){
			
		}
	}
	
	public String Nome() {
		return "";
	}
	
	public int Atk() {
		return 0;
	}
	
	public int Def() {
		return 0;
	}
	
	//*********Magia***********
	public void criarCarta(String nome, int atk, int def, int hp, int num){
		boolean confirm=this.validacao(num, getClass().getName());
		if(confirm){
			
		}
	}
		
	//Fim Polimorfismo
	
	public Jogador getJogador() {
		return jogador;
	}

	public void setJogador(Jogador jogador) {
		this.jogador = jogador;
	}

	public Deck getDeck() {
		return deck;
	}

	public void setDeck(Deck deck) {
		this.deck = deck;
	}

	public int getSenha(int senha) {
		if(senha==this.senha)
			return senha;
		else return 0;
	}

	public void setSenha(int antg, int senha) {
		if(this.senha==antg)
			this.senha = senha;
	}

	public boolean isTipo() {
		return tipo;
	}

	public void setTipo(boolean tipo) {
		this.tipo = tipo;
	}

	public int getSerie() {
		return serie;
	}

	public void setSerie(int serie) {
		this.serie = serie;
	}
	
	public boolean validacao(int num, String classe){
		boolean val=false;
		
		if(classe.equals("game_grupo10.entidades.Monstro")&& num <= 50 && num > 0){
			val=true;
		}else if(classe.equals("game_grupo10.entidades.Magia") && num<=80 &&num>61){
			val=true;
		}else if(classe.equals("game_grupo10.entidades.Terreno") && num<=61 && num>50){
			val=true;
		}
		
		return val;
	}

}
