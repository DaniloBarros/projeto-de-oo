package game_grupo10.entidades;

import java.util.ArrayList;
/*import java.util.ArrayDeque;
import java.util.Deque;*/

public class Deck {
	//private Carta carta = new Carta(0);
	
	private ArrayList<Carta> cartas = new ArrayList <Carta>();
	//private Deque<Carta> deque = new ArrayDeque <Carta>();
	
	Rand random = new Rand();
	
	private Monstro[] monstro = new Monstro[5];
	private Magia[] magia = new Magia[2];	
	private Jogador jogador;
	
	public Deck(Jogador jogador){
		this.jogador=jogador;
	}

	public Deck(boolean tipo, Jogador jogador) {
		this.jogador = jogador;
		criarCartas(tipo);
		shuffle();
		//deque.addAll(cartas);
	}	
	

	
	public void criarCartas(boolean tipo){
		
		if(tipo){
			
			for(int i=0;i<monstro.length;i++){
				this.monstro[i]=new Monstro(i);
					
			}
			for(int i=0;i<magia.length;i++){
				this.magia[i]=new Magia(i+62);
			}
			
			
			this.monstro[0].criarCarta("Esqueleto", Rand.VarMonster(3,1), Rand.VarMonster(2, 1), 1);
			this.monstro[1].criarCarta("Drag�o", Rand.VarMonster(15,15), Rand.VarMonster(10, 6), 2);
			this.monstro[2].criarCarta("C�o", Rand.VarMonster(5,3), Rand.VarMonster(3, 1), 3);
			this.monstro[3].criarCarta("Necromancer", Rand.VarMonster(13,6), Rand.VarMonster(5, 2), 4);
			this.monstro[4].criarCarta("Orc De Escudo", Rand.VarMonster(5,3), Rand.VarMonster(13, 9), 5);
			
			this.magia[0].criarCarta("Curandeiro", 0, 0, 100, 0+62);
			this.magia[1].criarCarta("Ceifador", 5, 2, 0, 1+62);
			
			for(int i=0;i<monstro.length;i++){
				cartas.add(monstro[i]);
				if(i<2)
					cartas.add(magia[i]);
				else
					;
			}
			
			
			
			for(int i=0; i< cartas.size();i++){
				cartas.get(i).setJogador(jogador);
				cartas.get(i).setDeck(this);
			}
			
		}else{
			
			for(int i=0;i<monstro.length;i++){
				this.monstro[i]=new Monstro(i);
			}
			
			for(int i=0;i<magia.length;i++){
				this.magia[i]=new Magia(i+62);
			}
			
			this.monstro[0].criarCarta("Alma", Rand.VarMonster(3,1), Rand.VarMonster(2, 1), 1);
			this.monstro[1].criarCarta("Grifo", Rand.VarMonster(15,8), Rand.VarMonster(10, 6), 2);
			this.monstro[2].criarCarta("Aguia", Rand.VarMonster(5,3), Rand.VarMonster(3, 1), 3);
			this.monstro[3].criarCarta("Angelical", Rand.VarMonster(13,6), Rand.VarMonster(5, 2), 4);
			this.monstro[4].criarCarta("Templario", Rand.VarMonster(5,3), Rand.VarMonster(13, 9), 5);
			
			this.magia[0].criarCarta("Curandeiro", 0, 0, 100, 0+62);
			this.magia[1].criarCarta("Luz", 5, 2, 0, 1+62);
			
			for(int i=0;i<monstro.length;i++){
				cartas.add(monstro[i]);
				if(i<2)
					cartas.add(magia[i]);
				else
					;
			}
			
			for(int i=0; i< cartas.size();i++){
				cartas.get(i).setJogador(jogador);
				cartas.get(i).setDeck(this);
			}
		}
		
	}
	
	private void shuffle() {
		
		for(int i=0;i<cartas.size();i++){
			int rand = random.Cards();
			
			Carta temp = cartas.get(i);
			cartas.set(i, cartas.get(rand));
			cartas.set(rand, temp);
		}
		
	}

	
	public void setJogador(Jogador jogador, int senha){
		if(senha==1234)
			this.jogador=jogador;
	}
	
	public Jogador getJogador(){
		return this.jogador;
	}

	public ArrayList<Carta> getCartas() {
		return cartas;
	}

	public void setCartas(ArrayList<Carta> cartas) {
		this.cartas = cartas;
	}
	/*
	public Deque<Carta> getDeque() {
		return deque;
	}
	
	public void setDeque(Deque<Carta> deque) {
		this.deque = deque;
	}	
	 */
	
	
}
