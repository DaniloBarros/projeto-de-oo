package game_grupo10.entidades;

import java.util.ArrayList;

public class Jogador {
	private String nome;
	private int hp;
	private boolean tipoDeck;

	private Deck deck;//Deck existe sem jogador? -N�o
	private ArrayList<Carta> mao= new ArrayList<Carta>();
	private Tabuleiro tabuleiro;
	
	public Jogador() {
		this.hp = 2000;
		this.nome = "vazio";
		this.tipoDeck = false;
		this.tabuleiro = new Tabuleiro(this);
	}
	
	public Jogador(String nome, boolean tipo, Deck deck) {
		this.hp = 2000;
		this.nome = nome;
		this.tipoDeck = tipo;
		this.tabuleiro = new Tabuleiro(this);
		this.deck=deck;
	}
	
	public boolean gerarMao(Carta mao){
		boolean sucess;
		
		if(this.mao.size()<5){
			this.mao.add(mao);
			sucess = true;
		}else{
			sucess = false;
			}
		
		return sucess;
	}
	
	private boolean virarCarta(boolean estado){
		//true = modo neutro(carta na vertical)
		//false = modo defensivo/ofensivo(carta na horizontal)
		if(estado==true){
			estado=false;
		}else estado=true;
		
		return estado;
	}
	
	public void invocarCarta(int i){		
		tabuleiro.invocarCarta(mao.get(i).getClass().getName(),
			mao.get(i));
		mao.remove(i);
		
	}
	
	public void desistir(){
		this.hp = 0;
		
	}
	
	public void comprarCarta(){
		mao.add(deck.getCartas().get(0));
		deck.getCartas().remove(0);
	}
	
	public void descartar(int i){
		tabuleiro.adicionarCemiterio(mao.get(i));
		mao.remove(i);
		
	}
	
	public Deck getDeck(){
		return deck;
	}
	
	public void setDeck(Deck deck){
		this.deck = deck;
	}
	
	public ArrayList<Carta> getMao() {
		return mao;
	}

	public void setMao(ArrayList<Carta> carta) {
		this.mao = carta;
	}	
	
	public Tabuleiro getTabuleiro() {
		return tabuleiro;
	}

	public void setTabuleiro(Tabuleiro tabuleiro) {
		this.tabuleiro = tabuleiro;
	}

	public boolean getTipoDeck(){
		return this.tipoDeck;
	}
	
	public void setTipoDeck(boolean tipoDeck){
		this.tipoDeck=tipoDeck;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getHp() {
		return hp;
	}

	public void setHp(int hp) {
		this.hp = hp;
	}
	
}
