package game_grupo10.entidades;

import java.util.ArrayList;

//Arrumar relacao com Jogador

public class Criar {
	//Quantidade de cartas para teste. Quando tivermos o jogo
	//balanceado esse valor ser� revisado.
	private Monstro[] monster = new Monstro[5];
	private Magia[] magia = new Magia[5];
	private Terreno[] terreno = new Terreno[5];
	private Jogador[] jogador = new Jogador[2];
	
	private ArrayList<Monstro> monsters = new ArrayList<Monstro>();
	
	public Criar(){
		
	}

	public Criar(boolean tipo, Jogador jogador, int i) {
		criarCarta(tipo, jogador);
		this.jogador[i]=jogador;
	}
	
	private void criarCarta(boolean tipo, Jogador jogador){
		//Cria��o dos Monstros
		if(tipo){
			for(int i=0;i<monster.length;i++)
				//this.monster[i]=new Monstro(i);
			
			
			this.monster[0].criarCarta("Esqueleto", Rand.VarMonster(3,1), Rand.VarMonster(2, 1), 1);
			this.monster[1].criarCarta("Drag�o", Rand.VarMonster(15,15), Rand.VarMonster(10, 6), 2);
			this.monster[2].criarCarta("C�o", Rand.VarMonster(5,3), Rand.VarMonster(3, 1), 3);
			this.monster[3].criarCarta("Necromancer", Rand.VarMonster(13,6), Rand.VarMonster(5, 2), 4);
			this.monster[4].criarCarta("Orc De Escudo", Rand.VarMonster(5,3), Rand.VarMonster(13, 9), 5);
			
			
			for(int i=0;i<monster.length;i++)
				this.monster[i].setJogador(jogador);
						
			for(int i=0;i<monster.length;i++)
				monsters.add(monster[i]);
			
		}else{
			for(int i=0;i<monster.length;i++)
				//this.monster[i]=new Monstro(i);
			
			this.monster[0].criarCarta("Alma", Rand.VarMonster(3,1), Rand.VarMonster(2, 1), 1);
			this.monster[1].criarCarta("Grifo", Rand.VarMonster(15,8), Rand.VarMonster(10, 6), 2);
			this.monster[2].criarCarta("Aguia", Rand.VarMonster(5,3), Rand.VarMonster(3, 1), 3);
			this.monster[3].criarCarta("Angelical", Rand.VarMonster(13,6), Rand.VarMonster(5, 2), 4);
			this.monster[4].criarCarta("Templario", Rand.VarMonster(5,3), Rand.VarMonster(13, 9), 5);
			
			this.monster[0].setJogador(jogador);
			this.monster[1].setJogador(jogador);
			this.monster[2].setJogador(jogador);
			this.monster[3].setJogador(jogador);
			this.monster[4].setJogador(jogador);
		}
		/*Magia
		this.magia[0].criarCarta("Sacrificio", bonus_1, bonus_2, 5);
		.
		.
		.
		*/
	}

	public Monstro getMonster(int i) {
		return monster[i];
	}

	public void setMonster(Monstro[] monster) {
		this.monster = monster;
	}

	public Magia[] getMagia() {
		return magia;
	}

	public void setMagia(Magia[] magia) {
		this.magia = magia;
	}

	public Terreno[] getTerreno() {
		return terreno;
	}

	public void setTerreno(Terreno[] terreno) {
		this.terreno = terreno;
	}

	public Jogador getJogador(int i) {
		return jogador[i];
	}

	public void setJogador(Jogador jogador, int i) {
		this.jogador[i] = jogador;
	}	

}
