package game_grupo10.teste;

//import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import game_grupo10.entidades.Jogador;
import game_grupo10.entidades.Monstro;

public class JogadorTest extends TestCase {
	
	public JogadorTest(){
		super();
	}


	@Before
	public void setUp() throws Exception {
		System.out.println("Iniciando...");
	}
	@After
	public void tearDown() throws Exception {
		System.out.println("Finalizando...");
	}
	@Test
	public void testGetCartaMonstro() {
		Jogador jogador = new Jogador();
		Monstro[] monster = new Monstro[1];
		monster[0]=new Monstro();
		monster[0].criarCarta("Aguia", 4, 5, 3);
		jogador.setCartaMonstro(monster);
		assertEquals(jogador.getCartaMonstro(0).getNum(),3);
	}

}
