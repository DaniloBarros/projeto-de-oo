package game_grupo10.teste;

//import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import game_grupo10.entidades.Monstro;

public class CartaTest extends TestCase {
	
	public CartaTest(){
		super();
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("Iniciando...");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Finalizando...");
	}

	@Test
	public void testGetNum() {
		Monstro Esqueleto = new Monstro(1);
		Esqueleto.criarCarta("Esqueleto", 5, 2, 1);
		assertEquals(Esqueleto.getNum(),1);
	}

}
