package entidades;

public class Magia extends Carta {
	
	private String nome;
	private int bonusatk;
	private int bonusdef;
	private int bonushp;
	private int num;

	public Magia(int num) {
		super(num);
		this.bonusatk = 0;
		this.bonusdef = 0;
		this.bonushp = 0;
		this.nome = "vazio";
		this.num=num;
	}
	
	@Override
	public void criarCarta(String nome, int atk, int def, int hp, int num){
	
		boolean confirm=this.validacao(num, this);
		
		if(confirm){
			this.bonusatk=atk;
			this.bonusdef=def;
			this.bonushp=hp;
			this.nome=nome;
			this.num=super.serie=num;
		}else{
			this.bonusatk = 0;
			this.bonusdef = 0;
			this.bonushp = 0;
			this.nome = "vazio";
			this.num=super.serie=-1;
		}
		
	}
	
	@Override
	public String toString(){
		return String.format("%s:\nBonus Attack: %d\nBonus Defense: %d" +
				"\nBonus HP: %d\nSerie Number: %d", this.nome,
				this.bonusatk, this.bonusdef, this.bonushp, this.serie);
	}
	
	@Override
	public String Nome() {
		return nome;
	}
	
	public int getNum(){
		return num;
	}
	
	public void setNum(int num){
		this.num = num;
	}

 	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getBonusatk() {
		return bonusatk;
	}

	public void setBonusatk(int bonusatk) {
		this.bonusatk = bonusatk;
	}

	public int getBonusdef() {
		return bonusdef;
	}

	public void setBonusdef(int bonusdef) {
		this.bonusdef = bonusdef;
	}

	public int getBonushp() {
		return bonushp;
	}

	public void setBonushp(int bonushp) {
		this.bonushp = bonushp;
	}
	
}
