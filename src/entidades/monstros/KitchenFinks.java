package entidades.monstros;

import entidades.Dark;
import entidades.interfaces.Terrestre;

public class KitchenFinks extends Dark implements Terrestre {

	public KitchenFinks() {
		super(49);
		this.atk=3;
		this.def=2;
		this.setNome("Kitchen Finks");
		this.setTipo(true);
	}
	
	@Override
	public int Atk(){		
		
		return this.atk;		
	}

	@Override
	public void efeito() {
		int hp = this.getJogador().getHp();
		hp += 2;
		getJogador().setHp(hp);
	}

	@Override
	public String descricao() {		
		return String.format("%s", "Quando Kitchen Finks entra na" +
				" batalha o Jogador ganha 2 de pontos de vida");
	}
	
}
