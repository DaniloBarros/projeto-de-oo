package entidades;

public class Terreno extends Carta{
	private int mana;
	private int num;
	

	public Terreno() {
		super(-1);
		int mana=0;
		int num=-1;
	}
	
	@Override
	public void criarCarta(int mana, int num){
		boolean confirm=this.validacao(num, this);
		if(confirm){
			this.mana=mana;
			this.num=super.serie=num;
		}else{
			this.mana=0;
			this.num=super.serie=-1;
		}
			
	}
	
	public int getNum(){
		return num;
	}
	
	public void setNum(int num){
		this.num=num;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}

}
