package entidades;

import java.util.Scanner;

import entidades.interfaces.EfeitoDescricao;

public abstract class Monstro extends Carta implements EfeitoDescricao {
	
	protected String nome;
	protected String descricao;
	protected int atk;
	protected int def;
	private Scanner input = new Scanner(System.in);

	public Monstro(int num) {
		super(num);
		this.atk = 0;
		this.def = 0;
		this.nome = "vazio";
		this.descricao = "vazio";
		
	}	
	
	/**Retirar Depois
	*/
	public void criarCarta(String nome, int atk, int def, int num, int var){
		
		this.atk = atk;
		this.def = def;
		this.nome = nome;	
		super.serie = num;

		boolean confirm=this.validacao(num, this);
		
		if(confirm==false){			
			this.atk = 0;
			this.def = 0;
			this.nome = "vazio";
			super.serie = -1;
		}else{
			criarDescricao(true);
		}
	}
	@Override
	public void criarCarta(String nome, int atk, int def, int num){
		
		this.atk = atk;
		this.def = def;
		this.nome = nome;	
		super.serie = num;

		boolean confirm=this.validacao(num, this);
		
		if(confirm==false){			
			this.atk = 0;
			this.def = 0;
			this.nome = "vazio";
			super.serie = -1;
		}else{
			criarDescricao(false);
		}
	}
	
	@Override
	public String toString(){
		return String.format("%s:\nAttack: %d\nDefense: %d\nSerie Number: %d", this.nome,
				this.atk, this.def, this.serie);
	}
	@Override
	public String Nome() {
		return nome;
	}
	
	/**Retirar Depois
	*/	
	private void criarDescricao(boolean pc){
		String desc;
		
		if (pc) {
			System.out.println("Insira a descri��o da carta: ");
			desc = input.nextLine();
		}else{
			desc="vazio";
		}
		this.setDescricao(desc);		
	}
		
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public int getAtk() {
		return atk;
	}

	public void setAtk(int atk) {
		this.atk = atk;
	}
	
	public int getDef() {
		return def;
	}

	public void setDef(int def) {
		this.def = def;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	

}
