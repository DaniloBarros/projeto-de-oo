package entidades;

import entidades.interfaces.EfeitoDescricao;

public class Carta implements EfeitoDescricao {

	private int senha = 1234;
	protected int serie;
	protected boolean tipo;
	
	private Deck deck;
	private Jogador jogador;
	
	
	public Carta(int serie) {
		this.serie=serie;
	}
	
	//Polimorfismo
	
	//*********Terrreno********
	public void criarCarta(int mana, int num){
		boolean confirm=this.validacao(num, this);
		if(confirm){
			
		}
	}
	
	//*********Monstro*********
	public void criarCarta(String nome, int atk, int def, int num){
		boolean confirm=this.validacao(num, this);
		if(confirm){
			
		}
	}
	
	public String Nome() {
		return "";
	}
	
	public int Atk() {
		return 0;
	}
	
	public int Def() {
		return 0;
	}
	
	//*********Magia***********
	public void criarCarta(String nome, int atk, int def, int hp, int num){
		boolean confirm=this.validacao(num, this);
		if(confirm){
			
		}
	}
		
	//Fim Polimorfismo
	
	public Jogador getJogador() {
		return jogador;
	}

	public void setJogador(Jogador jogador) {
		this.jogador = jogador;
	}

	public Deck getDeck() {
		return deck;
	}

	public void setDeck(Deck deck) {
		this.deck = deck;
	}

	public int getSenha(int senha) {
		if(senha==this.senha)
			return senha;
		else return 0;
	}

	public void setSenha(int antg, int senha) {
		if(this.senha==antg)
			this.senha = senha;
	}

	public boolean isTipo() {
		return tipo;
	}

	public void setTipo(boolean tipo) {
		this.tipo = tipo;
	}

	public int getSerie() {
		return serie;
	}

	public void setSerie(int serie) {
		this.serie = serie;
	}
	
	public boolean validacao(int num, Carta obj){
		boolean val=false;
		
		if(obj instanceof Monstro && num <= 50 && num > 0){
			val=true;
		}else if(obj instanceof Magia && num<=80 &&num>61){
			val=true;
		}else if(obj instanceof Terreno && num<=61 && num>50){
			val=true;
		}
		
		return val;
	}

	@Override
	public void efeito() {
		
	}

	@Override
	public String descricao() {
		
		return null;
	}

	@Override
	public void efeitoDescarte() {
		
	}

}
