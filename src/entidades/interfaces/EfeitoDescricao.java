package entidades.interfaces;

public interface EfeitoDescricao {
	
	public void efeito();
	
	public void efeitoDescarte();
	
	public String descricao();

}
