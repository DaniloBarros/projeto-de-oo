package main;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Iterator;

import entidades.Deck;
import entidades.Jogador;


public class Game_grupo10main {
	
	//Variaveis globais	
	private static Scanner input;

	static Jogador[] jogador = new Jogador[2];
	static Deck[] deck = new Deck[2];
	
	public Game_grupo10main() {
		
	}

	/**
	 //Testes, testes e mais testes
		
		//isInstanceOf checa a classe do objeto no array
		
		
		System.out.println("**********Cartas Mao**********\n");
		
		//jogador[0].comprarCarta();
			
		for(int i=0;i<2;i++){
			System.out.println("**********"+deck[i].getJogador().getNome()+"**********");			
			for(int j=0;j<jogador[i].getMao().size();j++){
				System.out.println("Carta "+(j+1) +": " +jogador[i].getMao().get(j)+"\n");
			}
		}
		
		for(int i=0;i<2;i++){
			System.out.println("**********"+deck[i].getJogador().getNome()+" Deck**********\n");
			for(int j=0;j<deck[i].getCartas().size();j++){
				System.out.println("Carta "+(j+1) +": " +deck[i].getCartas().get(j)+"\n");
			}
		}
	 */
	
	public static void main(String[] args) {
		input = new Scanner(System.in);
		
		inicializacao();
			
		for(int turn=0, i=0, cont=0; ; cont++){
			int opcao;		
			
			if(cont%2==0 && cont>0){
				 turn++;
			 }
			
			if(turn>0){
				jogador[i].comprarCarta();
				System.out.println("Adicionado " 
						+jogador[i].getMao().get( (jogador[i].getMao().size() ) - 1 ).Nome() 
						+" as cartas da m�o");
			}
			
		
			boolean passou=false;
			do{
				System.out.println("**********"+jogador[i].getNome()+"**********");
				opcao=menu();
				
				boolean test=false;
				int index;
				
				switch(opcao){
					case 0:
						System.out.println("Escolha uma carta a ser invocada.");
						maoImp(i);
						System.out.println("Escolha o numero v�lido de uma carta: ");
						index=input.nextInt();

						while(!test){
							try{
								jogador[i].invocarCarta(index);
								test=true;
								
							}catch(IndexOutOfBoundsException e){
								
								System.out.println("N�mero inv�lido!\n"
										+"Escolha o numero v�lido de carta: ");
								index=input.nextInt();
							}
						}
						break;

					case 1:System.out.println("Escolha uma carta a ser Descartada.");
						maoImp(i);
						System.out.println("Escolha o numero v�lido de uma carta: ");
						index=input.nextInt();					

						while(!test){					
							try{
								jogador[i].descartar(index);
								test=true;
								
							}catch(IndexOutOfBoundsException e){
								
								System.out.println("N�mero inv�lido!\n"
										+"Escolha o numero v�lido de carta: ");
								index=input.nextInt();
							}					
						}
						System.out.println("Carta adicionada ao Cemiterio");
						/* Impress�o de Cemiterio
						for(Iterator<Carta> itr = jogador[i].getTabuleiro().getCemiterio().iterator(); itr.hasNext(); )  {
							System.out.println(itr.next()+"/n");
						}
						*/
						break;

					case 2:System.out.println("'-'");
						break;
					case 3:System.out.println("o.o");
						break;
						
					case 4:
						System.out.println("Cartas Tabuleiro:\n"
								+jogador[0].getTabuleiro().toString());
						break;
						
					case 5:passou=true;
						break;

					case 6:System.out.println(jogador[i].getNome()+" Desistiu.");
						jogador[i].desistir();
						passou=true;
						break;
					
					default:System.out.println("ninja");

				}
			}while(!passou);
			
			if(i==1)
				i=0;
			else 
				i=1;
			
			if(jogador[0].getHp()==0){
				System.out.println(jogador[1].getNome()+" Ganhou!!");
				break;
			}
			if(jogador[1].getHp()==0){
				System.out.println(jogador[0].getNome()+" Ganhou!!");
				break;
			}
		}
		
		
	}
	
	private static void maoImp(int i){
					
		for(int j=0; j<jogador[i].getMao().size(); j++){
			System.out.println("\tCarta "+(j) +": \n" +jogador[i].getMao().get(j)+"\n");
		}
	}
	
	private static int menu(){
		input = new Scanner(System.in);
		int op;
		System.out.println("Digite uma op��o");
		System.out.println("0: Invocar Carta\n"+"1: Descartar Carta\n"
				+"2: Mudar Estado da Carta (Ativada/Desativada)\n"
				+"3: Atacar um inimigo\n"
				+"4: Exibir Cartas do Tabuleiro\n"+"5: Passar a Vez\n"
				+"6: Desistir");
		
		op=input.nextInt();
		
		while(op>6 || op<0){
			System.out.println("Op��o Invalida!");
			op=input.nextInt();
		}
		
		return op;
	}
	
	private static void inicializacao(){
		String opcao="n";
		String nao="n";
		String nome = "vazio";		
		int escolha=0;
		boolean tipo=false, teste=false;
		
		input = new Scanner(System.in);
		
		
		for(int i=0; i<jogador.length;i++){
			do{
				tipo=false;
				jogador[i]=new Jogador();
				System.out.println("Digite o nome no Jogador"+(i+1)+ ": ");
				nome = input.next();
				
				System.out.println("Escolha o tipo de Deck:\n1: Dark\n2: Light");				
				
				teste=false;
				while (!teste) {
					
					try {
						escolha = input.nextInt();
						teste=true;
					} catch (InputMismatchException e) {
						System.out.println("Entrada Invalida");
						escolha=0;
					}
				}
				
				if(escolha==1){
					tipo=true;
					
					if(i>0 && jogador[0].getTipoDeck()==true){
						tipo=false;
						
					}					
				}else{
					
					if(i>0 && jogador[0].getTipoDeck()==false){
						tipo=true;
						
					}	
				}
				
				jogador[i].setTipoDeck(tipo);
				
				jogador[i].setNome(nome);	
				nome = jogador[i].getNome();
				
				if(jogador[i].getTipoDeck()==true)
					System.out.println("Jogador "+(i+1)+": "+ nome +"\nTipo de Deck: Dark" +"\nOk? (s/n)");
				else
					System.out.println("Jogador "+(i+1)+": "+ nome +"\nTipo de Deck: Light" +"\nOk? (s/n)");
				
				teste=false;				
				while (!teste) {
					try{
						opcao = input.next();
						teste=true;
					}catch(InputMismatchException e){
						System.out.println("Entrada Invalida");
						opcao = input.next();
					}
				}
				
				deck[i] = new Deck(jogador[i].getTipoDeck(), jogador[i]);
				
				deck[i].setJogador(jogador[i], 1324);
				deck[i].getJogador().setNome(nome);
				
				jogador[i].setDeck(deck[i]);
				
				jogador[i]=new Jogador(nome, tipo, deck[i]);
				
			}while(opcao.equalsIgnoreCase(nao));
			
			
		}
		for(int i=0;i<2;i++){
			System.out.println("**********"+deck[i].getJogador().getNome()+"**********\n");
			for(int j=0;j<7;j++){
				System.out.println("Carta "+(j+1) +": " +deck[i].getCartas().get(j)+"\n");
			}
		}
		//System.out.println("\n**********Gerando M�o dos Jogadores**********\n\n");
		
		for(int i=0;i<2;i++){			
			for(int j=0;j<5;j++){
				if( jogador[i].gerarMao(deck[i].getCartas().get(0)) )
					deck[i].getCartas().remove(0);
					//deck[i].getDeque().pollFirst();
			}			
		}
		
	}
	
}