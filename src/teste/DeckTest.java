package teste;

import java.util.ArrayList;

import entidades.Carta;
import entidades.Deck;
import entidades.Jogador;
import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DeckTest extends TestCase {
	
	public DeckTest(){
		super();
	}

	@Before
	public void setUp() throws Exception {
		System.out.printf("%s", "Iniciando...");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Finalizando...");
	}

	@Test
	public void testGetJogador() {

		Jogador jogador = new Jogador();
		jogador.setNome("Danilo");
		Jogador jogador1 = new Jogador();
		jogador1.setNome("Seila");
		Deck deck = new Deck(true, jogador);
		deck.setJogador(jogador1, 1324);
		assertEquals(deck.getJogador(),jogador1);

	}

	@Test
	public void testGetCartas() {
		Jogador jogador = new Jogador();
		jogador.setNome("Danilo");
		Deck deck = new Deck(true, jogador);
		ArrayList<Carta> cartas = new ArrayList<Carta>();
		cartas = deck.getCartas();
		assertEquals(deck.getCartas(),cartas);
	}

}
