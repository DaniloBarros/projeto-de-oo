package teste;

import static org.junit.Assert.*;
import entidades.Carta;
import entidades.Deck;
import entidades.Jogador;
import entidades.monstros.KitchenFinks;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FinksTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetAtk() {
		KitchenFinks finks = new KitchenFinks();
		Jogador jogador = new Jogador();
		Deck deck = new Deck(jogador);
		jogador = new Jogador("Danilo",true,deck);
		ArrayList<Carta> carta = new ArrayList<Carta>();
		carta.add(finks);
		carta.get(0).setJogador(jogador);
		jogador.setMao(carta);
				
		System.out.println(jogador.getMao().get(0).descricao());
		System.out.println(jogador.getMao().get(0).getJogador().getNome()+" "+jogador.getMao().get(0).getJogador().getHp());
		jogador.getMao().get(0).efeito();
		System.out.println(jogador.getMao().get(0).getJogador().getNome()+" "+jogador.getMao().get(0).getJogador().getHp());
		assertEquals(jogador.getMao().get(0).Atk(),3);
	}

}
