package teste;

//import static org.junit.Assert.*;
import junit.framework.TestCase;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import entidades.Dark;
import entidades.Monstro;


public class CartaTest extends TestCase {
	
	public CartaTest(){
		super();
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("Iniciando...");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("Finalizando...");
	}

	@Test
	public void testGetNum() {
		Monstro Esqueleto = new Dark(1);
		Esqueleto.criarCarta("Esqueleto", 5, 2, 1);
		Esqueleto.criarCarta("Esqueleto", 1, 1, 1, 0);
		System.out.println(Esqueleto.getDescricao());
		assertEquals(Esqueleto.getSerie(),1);
	}

}
